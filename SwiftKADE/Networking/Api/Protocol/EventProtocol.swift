//
//  EventView.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//

import Foundation

protocol EventProtocol {
    func startLoading()
    func stopLoading()
    func setEmptyEvents()
    func getEvents(events: [Event])
}
