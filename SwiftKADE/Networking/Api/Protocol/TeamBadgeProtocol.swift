//
//  TeamBadgeView.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//

import Foundation

protocol TeamBadgeProtocol{
    func setHomeBadge(teams: [Team])
    func setAwayBadge(teams: [Team])
    
}
