//
//  EventPresenter.swift
//  SwiftKADE
//
//  Created by Pedro Ferreira on 17/08/21.
//
//MARK: Presentador que intermedia para obtener los Escudos mediante la funcion getTeams()
import Foundation

class EventPresenter{
    func getEvents(view:EventProtocol, service: EventService, url: String){
        view.startLoading()
        service.getEvents(url: url) { (events) in
            view.stopLoading()
            if events.count <= 0 {
                view.setEmptyEvents()
            } else {
                view.getEvents(events: events)
            }
        }
    }
}
