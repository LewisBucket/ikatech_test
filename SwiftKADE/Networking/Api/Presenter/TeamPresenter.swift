//
//  TeamPresenter.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//

// MARK: Presentador que intermedia para obtener los Escudos mediante la funcion getTeams()
import Foundation

class TeamPresenter{
    func getTeams(url: String, view:TeamBadgeProtocol, service: TeamService, isHome: Bool){
        service.getTeamBadge(url: url) { (teams) in
            if isHome {
                view.setHomeBadge(teams: teams)
            } else {
                view.setAwayBadge(teams: teams)
            }
        }
    }
}
