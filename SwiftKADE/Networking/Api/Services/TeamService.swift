//
//  TeamService.swift
//  SwiftKADE
//
//  Created by Pedro Ferreira on 17/08/21.
//

//MARK: Consumo de servicio para imagenes de los escudos de equipos.
import Foundation
import Alamofire
import SwiftyJSON

class TeamService{
    func getTeamBadge(url: String, _ completion:@escaping ([Team]) -> Void) {
        var teams = [Team]()
        Alamofire.request(url).responseJSON { (respons) in
            if respons.result.isSuccess {
                let teamJSON: JSON = JSON(respons.result.value!)
                for i in teamJSON["teams"].arrayValue {
                    let strTeamBadge = i["strTeamBadge"].stringValue
                    let team = Team(strTeamBadge: strTeamBadge)
                    teams.append(team)
                }
            } else if respons.result.isFailure {
                print("TeamService class : Failed")
            }
            completion(teams)
        }
    }
}
