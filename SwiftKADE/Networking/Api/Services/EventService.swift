//
//  EventService.swift
//  SwiftKADE
//  
//  Created by Pedro Ferreira on 17/08/21.
//

// MARK: Consumo del Servicio para listado de equipos.
import Foundation
import Alamofire
import SwiftyJSON

class EventService{
    func getEvents(url: String, _ completion:@escaping ([Event]) -> Void){
        var events = [Event]()
        Alamofire.request(url).responseJSON { (respons) in
            if respons.result.isSuccess {
                let eventsJSON: JSON = JSON(respons.result.value!)
                for item in eventsJSON["events"].arrayValue {
                    let strEvent = item["strEvent"].stringValue
                    let strSport = item["strSport"].stringValue
                    let idLeague = item["idLeague"].stringValue
                    let strLeague = item["strLeague"].stringValue
                    let strHomeTeam = item["strHomeTeam"].stringValue
                    let strHomeGoalDetails = item["strHomeGoalDetails"].stringValue
                    let strHomeLineupMidfield = item["strHomeLineupMidfield"].stringValue
                    let strHomeLineupForward = item["strHomeLineupForward"].stringValue
                    let strHomeLineupSubstitutes = item["strHomeLineupSubstitutes"].stringValue
                    let strAwayLineupForward = item["strAwayLineupForward"].stringValue
                    let dateEvent = item["dateEvent"].stringValue
                    let strDate = item["strDate"].stringValue
                    let strTime = item["strTime"].stringValue
                    let idHomeTeam = item["idHomeTeam"].stringValue
                    let idAwayTeam = item["idAwayTeam"].stringValue
                    let idEvent = item["idEvent"].stringValue
                    
                    let event = Event(strEvent: strEvent, strSport: strSport, idLeague: idLeague, strLeague: strLeague, strHomeTeam: strHomeTeam, strHomeGoalDetails: strHomeGoalDetails,   strHomeLineupMidfield: strHomeLineupMidfield, strHomeLineupForward: strHomeLineupForward, strHomeLineupSubstitutes: strHomeLineupSubstitutes, strAwayLineupForward: strAwayLineupForward, dateEvent: dateEvent, strDate: strDate, strTime: strTime, idHomeTeam:idHomeTeam, idAwayTeam: idAwayTeam, idEvent: idEvent)
                    events.append(event)
                }
            } else if respons.result.isFailure {
                print("EventService class : Failed")
            }
            completion(events)
        }
    }
}
