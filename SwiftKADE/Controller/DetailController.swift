//
//  DetailController.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK: DetailController actúa como intermediario entre el modelo Event y la vista Detail contenedora de un TableView. Aqui se guarda la data de la persistencia CoreData.
import UIKit
import CoreData

class DetailController: UIViewController {
    
    var event: Event!
    private var homeTeamLookupUrl: String!
    private var awayTeamLookupUrl: String!
    var context: NSManagedObjectContext!
    var eventsObject = [Events]()
    private var teamBadgePresenter = TeamPresenter()
    private var actionButton:UIBarButtonItem!
    private var button: UIButton!
    private var saveBarButton: UIBarButtonItem!
    private var isSaved:Bool = false
    private var detailView: DetailView! {
        guard isViewLoaded else { return nil }
        return view as! DetailView
    }
    
    
    // En estas funciones se chequea si es guardad la data para comparar con el estado del ButtonBar. El boton podra estar presionando almacenando y y vacio para eliminar el favorito.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detail Team"
        context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        detailView.configure(event: event)        
        checkIsSavedStatusFromCoreData()
        inflateBarButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        homeTeamLookupUrl = ListBadge + event.idHomeTeam
        teamBadgePresenter.getTeams(url: homeTeamLookupUrl!, view: self, service: TeamService(), isHome: true)
    }
    
    private func inflateBarButton(){
        actionButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: nil)
        
        button = UIButton(type: .custom)
        if isSaved {
            button.setImage(UIImage(named: "StarSaved"), for: .normal)
        } else {
            button.setImage(UIImage(named: "StarUnsaved"), for: .normal)
        }
        
        button.addTarget(self, action: #selector(saveBarButtonAction), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        saveBarButton = UIBarButtonItem(customView: button)
        let currWidth = saveBarButton.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = saveBarButton.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        
        self.navigationItem.setRightBarButtonItems([actionButton, saveBarButton], animated: true)
    }
    
    @objc private func saveBarButtonAction(){
        self.isSaved = !self.isSaved
        if self.isSaved {
            saveToCoreData()
        } else {
            deleteFromCoreData()
        }
        changeSaveBarButtonImage(isSaved: self.isSaved)
    }
    
    @objc private func changeSaveBarButtonImage(isSaved: Bool){
        self.navigationItem.rightBarButtonItems?.remove(at: 1)
        button = UIButton(type: .custom)
        
        if isSaved {
            button.setImage(UIImage(named: "StarSaved"), for: .normal)
        } else {
            button.setImage(UIImage(named: "StarUnsaved"), for: .normal)
        }
        button.addTarget(self, action: #selector(saveBarButtonAction), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        saveBarButton = UIBarButtonItem(customView: button)
        let currWidth = saveBarButton.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = saveBarButton.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        
        self.navigationItem.rightBarButtonItems?.insert(saveBarButton, at: 1)
    }
    
    func saveToCoreData(){
        let event = Events(context: context)
        event.dateEvent = self.event.dateEvent
        event.idAwayTeam = self.event.idAwayTeam
        event.idHomeTeam = self.event.idHomeTeam
        event.idLeague = self.event.idLeague
        event.strAwayLineupForward = self.event.strAwayLineupForward
        event.strDate = self.event.strDate
        event.strEvent = self.event.strEvent
        event.strHomeGoalDetails = self.event.strHomeGoalDetails
        event.strHomeLineupForward = self.event.strHomeLineupForward
        event.strHomeLineupMidfield = self.event.strHomeLineupMidfield
        event.strHomeLineupSubstitutes = self.event.strHomeLineupSubstitutes
        event.strHomeTeam = self.event.strHomeTeam
        event.strLeague = self.event.strLeague
        event.strSport = self.event.strSport
        event.strTime = self.event.strTime
        event.idEvent = self.event.idEvent
        
        do {
            try context.save()
        } catch let err as NSError{
            print ("Error \(err) with detail \(err.localizedDescription)")
        }
    }
    
    func deleteFromCoreData(){
        let fetchRequest: NSFetchRequest<Events> = Events.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "idEvent==\(self.event.idEvent)")
        
        do {
            let objects = try context.fetch(fetchRequest)
            for object in objects {
                context.delete(object)
            }
            try context.save()
        } catch _ {
            print("Error deleting")
        }
    }
    
    func checkIsSavedStatusFromCoreData(){
        let fetchRequest:NSFetchRequest<Events> = Events.fetchRequest()
        
        do {
            eventsObject = try context.fetch(fetchRequest)
        }catch{
            print(error)
        }
        
        for event in eventsObject {
            print(event.strHomeTeam! + " . " + event.strHomeTeam!)
            if event.idEvent == self.event.idEvent {
                self.isSaved = true
            }
        }
        print("")
    }
}

extension DetailController: TeamBadgeProtocol {
    func setHomeBadge(teams: [Team]) {
        let teamBadgeUrl = teams[0].strTeamBadge
        detailView.configureHomeBadge(url: teamBadgeUrl)
    }
    
    func setAwayBadge(teams: [Team]) {
        print("LOL")

    }
}

extension DetailController{
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
