//
//  LaunchController.swift
//  SwiftKADE
//
//  Created by Pedro Ferreira on 17/08/21.
//
//MARK: Animación para LaunchScreen no funcionó :(
import UIKit

class ViewController: UIViewController {
    
    private let imageViewLeague: UIImageView = {
        let imageViewLeague = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        imageViewLeague.image = UIImage(named: "logo")
        return imageViewLeague
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(imageViewLeague)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageViewLeague.center = view.center
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
            self.animate()
        })
    }
    
    private func animate() {
        UIView.animate(withDuration: 1, animations: {
            let size = self.view.frame.size.width * 3
            let diffX = size - self.view.frame.size.width
            let diffY = self.view.frame.size.height - size
            
            self.imageViewLeague.frame = CGRect(
                x: -(diffX/2),
                y: diffY/2,
                width: size,
                height: size
            )
            self.imageViewLeague.alpha = 0
        })
    }
    
}
