//
//  MainController.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK: MainController actúa como intermediario entre el modelo Event y la vista MainView contenedora de un TableView. MainController tambien gestiona los parametros de la celdas.
import UIKit
import CoreData

class MainController: UIViewController {
    
    private var events = [Event]()
    private var eventPresenter = EventPresenter()
    
    var context: NSManagedObjectContext!
    var eventsObject = [Events]()
    
    private var mainView: MainView! {
        guard isViewLoaded else { return nil }
        return view as! MainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Cuando carga se realiza la configuracion de los parametros de la celda, se inicia el BarButton "Back" y la variable que contendra la persistencia.
        self.title = "English Premiere League"
        context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let nib = UINib(nibName: "EventCell", bundle: nil)
        mainView.eventTableView.register(nib, forCellReuseIdentifier: "eventCell")
        
        mainView.eventTableView.delegate = self
        mainView.eventTableView.dataSource = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        
        // En esta funcion se obtiene la URL y la data con la integracion del presentador del modelo
        eventPresenter.getEvents(view: self, service: EventService(), url: ListTeams)
    }
    
    
    // Cuando aparezca la vista MainView con el controlador de Segmentos y este sea igual a 2 se llamara la funcion fetchDataFromCoreData() para mostrar los favoritos.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if mainView.segmentedControl.selectedSegmentIndex == 2 {
            fetchDataFromCoreData()
        }
    }
    // Configura los casos para cada Segmento. en el case 0: ingresa a la URL para la Lista de Equipos y en el case 1: llama a la funcion fetchDataFromCoreData()
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            eventPresenter.getEvents(view: self, service: EventService(), url: ListTeams)
        case 1:
            fetchDataFromCoreData()

        
        default:
            break
        }
    }
    
    
    //MARK: Esta funcion obtiene y almacenada la data en la persistencia de CoreData
    //la variable fetchRequest almacena los datos en <Events> siguiendo al metodo fecthRequest()
    func fetchDataFromCoreData(){
        self.events.removeAll()
        let fetchRequest:NSFetchRequest<Events> = Events.fetchRequest()
        do {
            eventsObject = try context.fetch(fetchRequest)
        } catch{
            print(error)
        }
        for event in eventsObject {
            let dateEvent = event.dateEvent!
            let idAwayTeam = event.idAwayTeam!
            let idHomeTeam = event.idHomeTeam!
            let idLeague = event.idLeague!
            let strAwayLineupForward = event.strAwayLineupForward!
            let strDate = event.strDate!
            let strEvent = event.strEvent!
            let strHomeGoalDetails = event.strHomeGoalDetails!
            let strHomeLineupForward = event.strHomeLineupForward!
            let strHomeLineupMidfield = event.strHomeLineupMidfield!
            let strHomeLineupSubstitutes = event.strHomeLineupSubstitutes!
            let strHomeTeam = event.strHomeTeam!
            let strLeague = event.strLeague!
            let strSport = event.strSport!
            let strTime = event.strTime!
            let idEvent = event.idEvent!
            
            let event = Event(strEvent: strEvent, strSport: strSport, idLeague: idLeague, strLeague: strLeague, strHomeTeam: strHomeTeam, strHomeGoalDetails: strHomeGoalDetails,  strHomeLineupMidfield: strHomeLineupMidfield, strHomeLineupForward: strHomeLineupForward, strHomeLineupSubstitutes: strHomeLineupSubstitutes, strAwayLineupForward: strAwayLineupForward, dateEvent: dateEvent, strDate: strDate, strTime: strTime, idHomeTeam:idHomeTeam, idAwayTeam: idAwayTeam, idEvent: idEvent)
            events.append(event)
        }
        mainView.eventTableView.reloadData()
    }
}

//MARK: Una extencion de los delegados para la Tabla.
extension MainController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as! EventCell
        cell.configure(event: events[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = DetailController(nibName: "DetailView", bundle: nil)
        controller.event = self.events[indexPath.row]
        navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MainController: EventProtocol{
    func startLoading() {
        mainView.loadingIndicator.isHidden = false
        mainView.loadingIndicator.startAnimating()
        mainView.eventTableView.isHidden = true
        mainView.errorLabel.isHidden = true
    }
    
    func stopLoading() {
        mainView.loadingIndicator.stopAnimating()
        mainView.loadingIndicator.isHidden = true
    }
    
    func setEmptyEvents() {
        mainView.eventTableView.isHidden = true
        mainView.errorLabel.isHidden = false
    }
    
    func getEvents(events: [Event]) {
        self.events.removeAll()
        mainView.eventTableView.isHidden = false
        mainView.errorLabel.isHidden = true
        self.events = events
        mainView.eventTableView.reloadData()
        
    }
}

extension MainController{
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
