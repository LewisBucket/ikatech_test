//
//  Team.swift
//  SwiftKADE2
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK: Modelos de las variables para Escudos
import Foundation

class Team {
    private var _strTeamBadge:String!
    
    public var strTeamBadge: String {
        return _strTeamBadge
    }
    
    init(strTeamBadge: String) {
        self._strTeamBadge = strTeamBadge
    }
}
