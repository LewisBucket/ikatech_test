//
//  Event.swift
//  SwiftKADE2
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK  Modelos de las variables, el tipo de datos y variables para la persistencia.
import Foundation

class Event{
    private var _strEvent:String!
    private var _strSport:String!
    private var _idLeague:String!
    private var _strLeague:String!
    private var _strHomeTeam:String!
    private var _strHomeGoalDetails:String!
    private var _strHomeLineupMidfield:String!
    private var _strHomeLineupForward:String!
    private var _strHomeLineupSubstitutes:String!
    private var _strAwayLineupForward:String!
    private var _dateEvent:String!
    private var _strDate:String!
    private var _strTime:String!
    private var _idHomeTeam:String!
    private var _idAwayTeam:String!
    private var _idEvent: String!
    
    public var idEvent:String {
        if let i = _idEvent {
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
    }
    
    public var strEvent:String {
        if let i = _strEvent {
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
    }
    
    public var strSport:String {
        if let i = _strSport{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
    }
    public var idLeague:String{
        if let i = _idLeague {
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var strLeague:String {
        if let i = _strLeague {
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var strHomeTeam:String{
        if let i = _strHomeTeam {
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    
    public var strHomeGoalDetails:String{
        if let i = _strHomeGoalDetails{
            if i == "" {
                return "-"
            }
            return i
        }
        return "null"
        
    }
    
    public var strHomeLineupMidfield:String{
        if let i = _strHomeLineupMidfield{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var strHomeLineupForward:String{
        if let i = _strHomeLineupForward{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var strHomeLineupSubstitutes:String{
        if let i = _strHomeLineupSubstitutes{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    
    public var strAwayLineupForward:String{
        if let i = _strAwayLineupForward{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    
    public var dateEvent:String{
        if let i = _dateEvent{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var strDate:String{
        if let i = _strDate{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var strTime:String{
        if let i = _strTime{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var idHomeTeam:String{
        if let i = _idHomeTeam{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
        
    }
    public var idAwayTeam:String{
        if let i = _idAwayTeam{
            if i == "" {
                return "null"
            }
            return i
        }
        return "null"
    }
    
    init(strEvent:String, strSport:String, idLeague:String, strLeague:String,
         strHomeTeam:String,
         strHomeGoalDetails:String,
         strHomeLineupMidfield:String,strHomeLineupForward:String, strHomeLineupSubstitutes:String,
         strAwayLineupForward:String,
         dateEvent:String, strDate:String, strTime:String, idHomeTeam:String, idAwayTeam:String, idEvent: String ) {
        
        self._strEvent = strEvent
        self._strSport = strSport
        self._idLeague = idLeague
        self._strLeague = strLeague
        self._strHomeTeam = strHomeTeam
        self._strHomeGoalDetails = strHomeGoalDetails
        self._strHomeLineupMidfield = strHomeLineupMidfield
        self._strHomeLineupForward = strHomeLineupForward
        self._strHomeLineupSubstitutes = strHomeLineupSubstitutes
        self._strAwayLineupForward = strAwayLineupForward
        self._dateEvent = dateEvent
        self._strDate = strDate
        self._strTime = strTime
        self._idHomeTeam = idHomeTeam
        self._idAwayTeam = idAwayTeam
        self._idEvent = idEvent
    }
}
