//
//  DetailView.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK: Clase de la vista de Detalles.
import UIKit
import Kingfisher

class DetailView: UIView {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var homeImageView: UIImageView!
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var homeScoreLabel: UILabel!
    @IBOutlet weak var awayScoreLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var awayImageView: UIImageView!
    @IBOutlet weak var homeGoalScorerLabel: UILabel!
    @IBOutlet weak var awayGoalScorerLabel: UILabel!
    @IBOutlet weak var homeGoalKeeperLabel: UILabel!
    @IBOutlet weak var awayGoalKeeperLabel: UILabel!
    @IBOutlet weak var homeDefenseLabel: UILabel!
    @IBOutlet weak var awayDefenseLabel: UILabel!
    @IBOutlet weak var homeMidFielderLabel: UILabel!
    @IBOutlet weak var awayMidFielderLabel: UILabel!
    @IBOutlet weak var homeForwardLabel: UILabel!
    @IBOutlet weak var awayForwardLabel: UILabel!
    @IBOutlet weak var homeSubstitutesLabel: UILabel!
    @IBOutlet weak var awaySubstitutesLabel: UILabel!
    @IBOutlet weak var homeBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var awayBottomConstraint: NSLayoutConstraint!
  
    // Sobre esta funcion se controlan los Labels que se muestran en la DetailView
    func configure(event: Event){
        self.dateLabel.text = event.strHomeTeam
        self.homeTeamLabel.text = event.strHomeTeam
      
//        self.homeGoalScorerLabel.text = event.strHomeTeam.replacingOccurrences(of: ";", with: "\n", options: .literal, range: nil)
//
//        self.homeMidFielderLabel.text = event.strTime.replacingOccurrences(of: "; ", with: "\n", options: .literal, range: nil)
//
//        self.homeForwardLabel.text = event.strHomeLineupForward.replacingOccurrences(of: "; ", with: "\n", options: .literal, range: nil)
//        //self.awayForwardLabel.text = event.strAwayLineupForward.replacingOccurrences(of: "; ", with: "\n", options: .literal, range: nil)
//
//        self.homeSubstitutesLabel.text = event.strHomeLineupSubstitutes.replacingOccurrences(of: "; ", with: "\n", options: .literal, range: nil)
    }
    //  configureHomeBadge() Imprime el Escudo como imagen usando la libreria Kingfisher( .kf)
    func configureHomeBadge(url: String){
        self.homeImageView.kf.setImage(with: URL(string: url))
    }
    
//    func configureAwayBadge(url: String){
//        self.awayImageView.kf.setImage(with: URL(string: url))
//    }
}
