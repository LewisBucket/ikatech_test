//
//  MainView.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK: Clase de la vista principal.
import UIKit

class MainView: UIView {
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
}
