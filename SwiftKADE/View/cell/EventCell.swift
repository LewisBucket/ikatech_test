//
//  EventCell.swift
//  SwiftKADE
//
// Created by Pedro Ferreira on 17/08/21.
//
// MARK: Controla los objetos de la vista de la Celda "EventCell.xib"
import UIKit

class EventCell: UITableViewCell {
    @IBOutlet weak var dateEventLabel: UILabel!
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var homeTeamScoreLabel: UILabel!
    @IBOutlet weak var awayTeamScore: UILabel!
    @IBOutlet weak var viewContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    // La funcion configure() entrega parametros a la variables de Label HomeTeamLabel
    func configure(event: Event){
        homeTeamLabel.text = event.strHomeTeam
        
        
       
    }
}
